# Copyright 2022-2023, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0
"""Configuration for "copybara" <https://github.com/google/copybara> to update vendored source"""

gitlab_url = "git@gitlab.freedesktop.org:ryan.pavlik/openxr-android-broker.git"
author = "Rylie Pavlik <rylie.pavlik@collabora.com>"

# update-jnipp: Update jnipp
core.workflow(
    name = "update-jnipp",
    origin = git.github_origin(
        url = "https://github.com/mitchdowd/jnipp.git",
        ref = "master",
    ),
    destination = git.destination(
        url = gitlab_url,
        fetch = "main",
        push = "update-jnipp",
    ),
    destination_files = glob(["vendor/jnipp/**"]),
    origin_files = glob(["**"]),
    authoring = authoring.pass_thru(author),
    transformations = [
        metadata.expose_label("GIT_DESCRIBE_CHANGE_VERSION"),
        metadata.replace_message("vendor/jnipp: Update jnipp from upstream ${GIT_DESCRIBE_CHANGE_VERSION}"),
        core.move("", "vendor/jnipp/"),
    ],
)

# update-jni-wrappers: Update android-jni-wrappers
# TODO does not work right? maybe it does?
core.workflow(
    name = "update-jni-wrappers",
    origin = git.origin(
        url = "https://gitlab.freedesktop.org/monado/utilities/android-jni-wrappers.git",
        ref = "main",
    ),
    destination = git.destination(
        url = gitlab_url,
        fetch = "main",
        push = "update-jni-wrappers",
    ),
    destination_files = glob(["vendor/android-jni-wrappers/wrap/*"]),
    origin_files = glob(["wrap/*", "README.md"]),
    authoring = authoring.pass_thru(author),
    transformations = [
        metadata.expose_label("GIT_DESCRIBE_CHANGE_VERSION"),
        metadata.replace_message("vendor/android-jni-wrappers: Update android-jni-wrappers from upstream ${GIT_DESCRIBE_CHANGE_VERSION}"),
        core.move("wrap/", "vendor/android-jni-wrappers/wrap/"),
        core.move("README.md", "vendor/android-jni-wrappers/wrap/README.md"),
    ],
)
