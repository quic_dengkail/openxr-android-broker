#!/usr/bin/env bash
# SPDX-License-Identifier: CC0-1.0
# SPDX-FileCopyrightText: 2018-2023 Collabora, Ltd. and the Monado contributors
set -euo pipefail

# Yes, this is overkill: there is no reason that the official icons need to be
# behind 3072-bit RSA encryption, especially when "officialness" is really
# granted by the signing keys. I just was not 100% sure of the redistribution terms
# of the logo-related images (they keep pointing me to the trademark rules,
# and I keep asking for the copyright license, not the trademark license),
# and this prevents accidental building of packages with official-looking icons.

CLEAR_FN=official.tar.xz

cd "$(dirname "$0")/.."
cd installable_runtime_broker/src/

# download and decrypt the archive
# using sq - sequoia - which is more stateless than normal gnupg and thus easier for CI
curl --silent https://people.collabora.com/~rpavlik/ci_resources/broker/$CLEAR_FN.gpg | \
  sq decrypt --recipient-key "${GPG_SECRET_KEY}" > $CLEAR_FN

# unpack it
tar xvf $CLEAR_FN

# remove the compressed file
rm -f $CLEAR_FN
