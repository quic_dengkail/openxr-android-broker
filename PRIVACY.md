# Privacy when using the OpenXR™ Runtime Broker (installable)

<!--
Copyright 2022, Collabora, Ltd.
SPDX-License-Identifier: CC-BY-4.0
-->

The OpenXR Runtime Broker application for Android™ (installable from app markets
or a standalone APK file) is an essential part of using OpenXR on that platform.
This application is designed for user choice, freedom, and privacy. It does not
transmit any data to its developers, does not record any analytics or telemetry,
and does not request or have internet access. (That is, no data is "collected".)
The only data used by the app is the data it requires to perform its function,
and it never leaves your device. It lets you choose at most one
application/package to mark as your "Active OpenXR Runtime", and provides only
the data required to access that active runtime to querying applications (i.e.
OpenXR applications/experiences) that are authorized to contact it by the
operating system. If you do not also install a runtime and explicitly enable it
in the OpenXR Runtime Broker, it will report only that it cannot provide an
active runtime.

## Details

As a requirement of how it works, the OpenXR Runtime Broker queries your device
for all packages that report the standardized metadata that indicates an OpenXR
runtime, to be able to provide you with a choice in which software to use. This
data on installed runtimes is not shared with anyone, including any app, and
never leaves your device.

No runtime is active by default in the OpenXR Runtime Broker. If you choose to
make a runtime active, as a requirement of how it works, the OpenXR Runtime
Broker will disclose to a requesting application the identity of your chosen
active runtime. Where applicable (based on Android™ version), access to this
data is restricted to applications that have declared in their manifest that
they will query the runtime broker. This is required for normal usage of OpenXR
applications, but other applications may also add this and the broker has no way
of knowing how the application will use the data.

The broker cannot, and is not intended to, circumvent technological or policy
restrictions in Android or your device. Its purpose is to provide a secure
method to select your active runtime and provide the information to implement
your choice to requesting applications.

## Precautions

Packages listed in the user interface of the OpenXR Runtime Broker include all
those that indicate that they can provide an implementation of OpenXR. There is
no verification or validation done to limit this list, and it may include
runtimes that are not conformant with the OpenXR standard, as well as packages
that might expose this metadata maliciously. It remains your responsibility to
choose your active runtime carefully. This is typically an infrequent practice
as it often only changes when you change which XR hardware you are using.

The OpenXR Runtime Broker is only a way for you to choose what OpenXR runtime
you want to use, and for applications to access this preference. Once the
"introduction" is done, the runtime broker is no longer involved. Your privacy
while using an OpenXR application depends on the application/experience you run
and the OpenXR runtime you are using (usually provided by an XR device vendor).

XR experiences can involve personal data and sensor measurements: motion
sensing, cameras, etc. Be sure that you have read the Privacy Policies of
applications and your runtime, and that you trust the application with the
motion data (and other data) it may be able to access. Your chosen OpenXR
runtime may provide permissions or privacy settings to limit functionality
accessible by some applications, and system privacy controls regarding e.g.
location and camera access are typically still in effect in XR applications.

## System OpenXR Runtime Broker

Some devices intended by their manufacturer to support OpenXR may ship with a
"system" OpenXR Runtime Broker. It behaves similarly to the installable runtime
broker, in that permitted applications that contact it may receive details about
using an active runtime. However, this "system" broker is maintained by your
device manufacturer who is ultimately responsible for it: the OpenXR Working
Group only provides an open source template to start development of such a
package.

Applications that use OpenXR on Android conventionally search first for an
installable OpenXR Runtime Broker. If one is not found, or if it reports that no
active runtime is indicated, a System OpenXR Runtime Broker is queried if
present, followed by other system locations. Runtimes accessed through a system
runtime broker may be active by default, unlike in the installable broker, and
are not necessarily modifiable, depending on the choices the implementing vendor
made.

## Trademarks

OpenXR™ and the OpenXR logo are trademarks owned by The Khronos Group Inc. and
are registered as a trademark in China, the European Union, Japan and the United
Kingdom.

Khronos and the Khronos Group logo are registered trademarks of the Khronos
Group Inc.

Android is a trademark of Google LLC.

All other product names, trademarks, and/or company names are used solely for
identification and belong to their respective owners.
