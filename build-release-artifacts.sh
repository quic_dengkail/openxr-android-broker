#!/bin/sh
# Copyright 2020-2023, Collabora, Ltd.
#
# SPDX-License-Identifier: BSL-1.0

(
    cd "$(dirname "$0")"
    set -e

    ./gradlew clean
    ./gradlew installable_runtime_broker:assembleOfficialRelease installable_runtime_broker:bundleOfficialRelease

)
